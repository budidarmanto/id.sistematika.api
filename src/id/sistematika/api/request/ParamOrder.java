package id.sistematika.api.request;

import java.math.BigDecimal;
import java.util.List;

public class ParamOrder {
    Integer ad_org_id;
    String poreference;
    Integer c_doctype_id;
    String dateordered;
    
    Integer m_warehouse_id;
    Integer m_pricelist_id;
    Integer salesrep_id;
    String payment_rule;
    List<ParamOrderLine> list_line;
    Integer c_bpartner_id;
    Integer c_order_id;
    
    //diambil dari forca crm impl
    // mandatory
    Integer c_bpartner_location_id;
    Integer bill_bpartner_id;
    Integer bill_location_id;
    Integer c_currency_id;
    
    String description;
    Integer c_paymentterm_id;
    BigDecimal discountamt;
    
    String issotrx;

    public BigDecimal getDiscountamt() {
        return discountamt;
    }

    public void setDiscountamt(BigDecimal discountamt) {
        this.discountamt = discountamt;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public String getPoreference() {
        return poreference;
    }

    public void setPoreference(String poreference) {
        this.poreference = poreference;
    }

    public Integer getC_doctype_id() {
        return c_doctype_id;
    }

    public void setC_doctype_id(Integer c_doctype_id) {
        this.c_doctype_id = c_doctype_id;
    }

    public String getDateordered() {
        return dateordered;
    }

    public void setDateordered(String dateordered) {
        this.dateordered = dateordered;
    }

    public Integer getM_warehouse_id() {
        return m_warehouse_id;
    }

    public void setM_warehouse_id(Integer m_warehouse_id) {
        this.m_warehouse_id = m_warehouse_id;
    }

    public Integer getM_pricelist_id() {
        return m_pricelist_id;
    }

    public void setM_pricelist_id(Integer m_pricelist_id) {
        this.m_pricelist_id = m_pricelist_id;
    }

    public Integer getSalesrep_id() {
        return salesrep_id;
    }

    public void setSalesrep_id(Integer salesrep_id) {
        this.salesrep_id = salesrep_id;
    }

    public String getPayment_rule() {
        return payment_rule;
    }

    public void setPayment_rule(String payment_rule) {
        this.payment_rule = payment_rule;
    }

    public List<ParamOrderLine> getList_line() {
        return list_line;
    }

    public void setList_line(List<ParamOrderLine> list_line) {
        this.list_line = list_line;
    }

    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public Integer getC_bpartner_location_id() {
        return c_bpartner_location_id;
    }

    public void setC_bpartner_location_id(Integer c_bpartner_location_id) {
        this.c_bpartner_location_id = c_bpartner_location_id;
    }

    public Integer getBill_bpartner_id() {
        return bill_bpartner_id;
    }

    public void setBill_bpartner_id(Integer bill_bpartner_id) {
        this.bill_bpartner_id = bill_bpartner_id;
    }

    public Integer getBill_location_id() {
        return bill_location_id;
    }

    public void setBill_location_id(Integer bill_location_id) {
        this.bill_location_id = bill_location_id;
    }

    public Integer getC_currency_id() {
        return c_currency_id;
    }

    public void setC_currency_id(Integer c_currency_id) {
        this.c_currency_id = c_currency_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getC_paymentterm_id() {
        return c_paymentterm_id;
    }

    public void setC_paymentterm_id(Integer c_paymentterm_id) {
        this.c_paymentterm_id = c_paymentterm_id;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }
    
    
}
