package id.sistematika.api.request;

import java.math.BigDecimal;

public class ParamOrderLine {
    Integer m_product_id;
    BigDecimal qty_entered;
    BigDecimal qty_bonusses;
    Integer c_tax_id;
    Integer c_uom_id;
    BigDecimal price_entered;
    BigDecimal list_price;
    BigDecimal discount;
    Integer c_charge_id;
    Integer c_orderline_id;
    
    public BigDecimal getQty_bonusses() {
        return qty_bonusses;
    }
    public void setQty_bonusses(BigDecimal qty_bonusses) {
        this.qty_bonusses = qty_bonusses;
    }
    public Integer getC_orderline_id() {
        return c_orderline_id;
    }
    public void setC_orderline_id(Integer c_orderline_id) {
        this.c_orderline_id = c_orderline_id;
    }
    public Integer getM_product_id() {
        return m_product_id;
    }
    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }
    public Integer getC_tax_id() {
        return c_tax_id;
    }
    public BigDecimal getQty_entered() {
        return qty_entered;
    }
    public void setQty_entered(BigDecimal qty_entered) {
        this.qty_entered = qty_entered;
    }
    public void setC_tax_id(Integer c_tax_id) {
        this.c_tax_id = c_tax_id;
    }
    public Integer getC_uom_id() {
        return c_uom_id;
    }
    public void setC_uom_id(Integer c_uom_id) {
        this.c_uom_id = c_uom_id;
    }
    public BigDecimal getPrice_entered() {
        return price_entered;
    }
    public void setPrice_entered(BigDecimal price_entered) {
        this.price_entered = price_entered;
    }
    public BigDecimal getList_price() {
        return list_price;
    }
    public void setList_price(BigDecimal list_price) {
        this.list_price = list_price;
    }
    public BigDecimal getDiscount() {
        return discount;
    }
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
    public Integer getC_charge_id() {
        return c_charge_id;
    }
    public void setC_charge_id(Integer c_charge_id) {
        this.c_charge_id = c_charge_id;
    }
    
}
