package id.sistematika.api.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;

import org.compiere.db.Database;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

import id.sistematika.api.request.ParamBPGroup;
import id.sistematika.api.request.ParamRequest;
import id.sistematika.api.response.ResponseData;
import id.sistematika.api.util.ProcessUtil;

public class MasterImpl {
    private CLogger log = CLogger.getCLogger(getClass());
    PreparedStatement statement = null;
    ResultSet rs = null;
    AuthenticationImpl authLoginImpl = new AuthenticationImpl();
    private static final String NATIVE_MARKER = "NATIVE_" + Database.DB_POSTGRESQL + "_KEYWORK";

    public ResponseData getBPGroup(ParamRequest param) {
        List<Object> params = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        StringBuilder whereCond = new StringBuilder();
        StringBuilder select = new StringBuilder();
        StringBuilder join = new StringBuilder();

        PreparedStatement ps = null;
        ResultSet rs = null;
        ResponseData resp = new ResponseData();
        List<Object> listdata = new ArrayList<Object>();
        LinkedHashMap<String, Object> map;


        //@formatter:off
        select.append(
                "SELECT " + 
                "    a.AD_Client_ID, " + 
                "    b.Name client_name, " + 
                "    a.AD_Org_ID, " + 
                "    a.Name, " +
                "    a.Value, " +
                "    a.C_BP_Group_ID, " +
                "    a.Description " 
                );
        join.append(
                "FROM C_BP_Group a " + 
                "LEFT JOIN AD_Client b ON " + 
                        "b.AD_Client_ID = a.AD_Client_ID " );
        whereCond.append(
                "WHERE a.C_BP_Group_ID IS NOT NULL "
                + " AND a.AD_Client_ID = ? ");
        //@formatter:on

        params.add(Env.getAD_Client_ID(Env.getCtx()));
        Integer c_bp_group_id = param.getC_bp_group_id();
        if (param.getC_bp_group_id() != null) {
            whereCond.append(" AND a.C_BP_Group_ID = ? ");
            params.add(c_bp_group_id);
        }

        sql.append(select.toString());
        sql.append(join.toString());
        sql.append(whereCond.toString());

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                map = new LinkedHashMap<String, Object>();
                map.put("ad_client_id", rs.getString("ad_client_id"));
                map.put("client_name", rs.getString("client_name"));
                map.put("c_bp_group_id", rs.getString("c_bp_group_id"));
                map.put("ad_org_id", rs.getString("ad_org_id"));
                map.put("value", rs.getString("value"));
                map.put("description", rs.getString("description"));
                map.put("name", rs.getString("name"));
                listdata.add(map);
            }

            resp = ResponseData.successResponse(listdata.size() + " Data Found", listdata);

        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return resp;
    }
    
    public ResponseData getBPGroupJSON(ParamBPGroup param) {
        List<Object> params = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        StringBuilder whereCond = new StringBuilder();
        StringBuilder select = new StringBuilder();
        StringBuilder join = new StringBuilder();

        PreparedStatement ps = null;
        ResultSet rs = null;
        ResponseData resp = new ResponseData();
        List<Object> listdata = new ArrayList<Object>();
        LinkedHashMap<String, Object> map;


        //@formatter:off
        select.append(
                "SELECT " + 
                "    a.AD_Client_ID, " + 
                "    b.Name client_name, " + 
                "    a.AD_Org_ID, " + 
                "    a.Name, " +
                "    a.Value, " +
                "    a.C_BP_Group_ID, " +
                "    a.Description " 
                );
        join.append(
                "FROM C_BP_Group a " + 
                "LEFT JOIN AD_Client b ON " + 
                        "b.AD_Client_ID = a.AD_Client_ID " );
        whereCond.append(
                "WHERE a.C_BP_Group_ID IS NOT NULL "
                + " AND a.AD_Client_ID = ? ");
        //@formatter:on

        params.add(Env.getAD_Client_ID(Env.getCtx()));
        Integer c_bp_group_id = param.getC_bp_group_id();
        if (param.getC_bp_group_id() > 0) {
            whereCond.append(" AND a.C_BP_Group_ID = ? ");
            params.add(c_bp_group_id);
        }

        sql.append(select.toString());
        sql.append(join.toString());
        sql.append(whereCond.toString());

        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps = ProcessUtil.setMultiParam(ps, params);
            if (ps.getConnection().getAutoCommit()) {
                ps.getConnection().setAutoCommit(false);
            }
            ps.setFetchSize(100);
            rs = ps.executeQuery();

            while (rs.next()) {
                map = new LinkedHashMap<String, Object>();
                map.put("ad_client_id", rs.getString("ad_client_id"));
                map.put("client_name", rs.getString("client_name"));
                map.put("c_bp_group_id", rs.getString("c_bp_group_id"));
                map.put("ad_org_id", rs.getString("ad_org_id"));
                map.put("value", rs.getString("value"));
                map.put("description", rs.getString("description"));
                map.put("name", rs.getString("name"));
                listdata.add(map);
            }

            resp = ResponseData.successResponse(listdata.size() + " Data Found", listdata);

        } catch (SQLException e) {
            log.log(Level.SEVERE, sql.toString(), e);
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } finally {
            DB.close(rs, ps);
            ps = null;
            rs = null;
        }
        return resp;
    }

}
