package id.sistematika.api.impl;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.db.Database;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import id.sistematika.api.model.X_POS_Product;
import id.sistematika.api.request.ParamBPGroup;
import id.sistematika.api.request.ParamOrder;
import id.sistematika.api.request.ParamOrderLine;
import id.sistematika.api.request.ParamRequest;
import id.sistematika.api.response.ResponseData;
import id.sistematika.api.util.ProcessUtil;
import id.sistematika.api.util.WsUtil;

public class TransactionImpl {
    private CLogger log = CLogger.getCLogger(getClass());
    PreparedStatement statement = null;
    ResultSet rs = null;
    AuthenticationImpl authLoginImpl = new AuthenticationImpl();
    private static final String NATIVE_MARKER = "NATIVE_" + Database.DB_POSTGRESQL + "_KEYWORK";
    ResponseData resp = new ResponseData();
    List<Object> listData = new ArrayList<>();
    WsUtil wu = new WsUtil();

    public ResponseData setSalesOrder(ParamOrder param, Trx trx) {
        listData.clear();
        int m_product_id = 0;
        Integer sales_booking_id = 0;
        
        MOrder o = null;
        try {
            o = new MOrder(Env.getCtx(), param.getC_order_id(), trx.getTrxName());
            o.setAD_Org_ID(param.getAd_org_id());
            o.setPOReference(param.getPoreference());
            o.setC_DocTypeTarget_ID(param.getC_doctype_id());
            o.setDateOrdered(wu.convertFormatTgl(param.getDateordered()));
            o.setC_BPartner_ID(param.getC_bpartner_id());
            o.setBill_BPartner_ID(param.getBill_bpartner_id());
            o.setC_BPartner_Location_ID(param.getBill_location_id());
            o.setM_Warehouse_ID(param.getM_warehouse_id());
            o.setM_PriceList_ID(param.getM_pricelist_id());
            o.setC_Currency_ID(param.getC_currency_id());
            o.setSalesRep_ID(param.getSalesrep_id());
            o.setPaymentRule(param.getPayment_rule());
            o.setC_PaymentTerm_ID(param.getC_paymentterm_id());
            o.setIsSOTrx(param.getIssotrx().equalsIgnoreCase("Y") ? true : false);
            o.setDiscountAmt(param.getDiscountamt());
            o.setIsApproved(false);
            o.saveEx();

            for (ParamOrderLine pol : param.getList_line()) {
                MOrderLine ol = new MOrderLine(Env.getCtx(), pol.getC_orderline_id(), trx.getTrxName());
                ol.setAD_Org_ID(o.getAD_Org_ID());
                ol.setC_Order_ID(o.get_ID());
                ol.setM_Product_ID(pol.getM_product_id());
                ol.setQtyOrdered(pol.getQty_entered());
                ol.setQtyEntered(pol.getQty_entered());
                ol.setC_UOM_ID(pol.getC_uom_id());
                ol.setQtyBonuses(pol.getQty_bonusses());
                ol.setPriceActual(pol.getPrice_entered());
                ol.setPriceEntered(pol.getPrice_entered());
                ol.setPriceList(pol.getList_price());
                ol.setC_Tax_ID(pol.getC_tax_id());
                ol.setDiscount(pol.getDiscount());
                ol.saveEx();
                m_product_id = ol.getM_Product_ID();
            }

            o.setDocAction(DocAction.ACTION_Complete);
            o.processIt(DocAction.ACTION_Complete);
            o.saveEx();

            if (!o.getDocStatus().equalsIgnoreCase(DocAction.STATUS_Completed)) {
                throw new AdempiereException("Error complete document");
            } else {
                
                //Insert to POS
                List<Object> params = new ArrayList<>();
                //@formatter:off
                String sql = 
                        "SELECT " + 
                        "    pos_product_id_ref " + 
                        "FROM pos_product " + 
                        "WHERE m_product_id = ? ";
                //@formatter:on
                params.add(m_product_id);
                Integer product_pos = DB.getSQLValueEx(null, sql, params);

                String url = "https://qp.forca.id/api/v1/distributor/auth/login";
                Map<String, String> mapHeader = new LinkedHashMap<>();
                mapHeader.put("Content-Type", "aplication/json");
                mapHeader.put("Accept", "aplication/json");
                JSONObject joUser = new JSONObject();
                String token = "";
                

                joUser.put("username", "developersisi2@gmail.com");
                joUser.put("password", "12345678");
                String respon = wu.requestHTTP("POST", url, joUser, mapHeader);
                JSONObject joToken = new JSONObject(respon);
                if (joToken.getString("status").equalsIgnoreCase("success")) {
                    JSONObject jo = joToken.getJSONObject("data");
                    token = jo.getString("token");

                    mapHeader.put("Forca-Token", token);
                    url = "https://qp.forca.id/api/v1/distributor/sales_booking/add_sales_booking";
                    JSONObject joP = new JSONObject();
                    JSONArray jaP = new JSONArray();
                    JSONObject joSB = new JSONObject();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    joP.put("product_id", String.valueOf(product_pos));
                    joP.put("price", "49000");
                    joP.put("quantity", "100");
                    joP.put("discount", "0");
                    jaP.put(joP);

                    joSB.put("date", sdf.format(new Date()));
                    joSB.put("warehouse", "3");
                    joSB.put("customer", "7");
                    joSB.put("order_discount", "");
                    joSB.put("shipping", "");
                    joSB.put("sale_status", "reserved");
                    joSB.put("payment_term", "");
                    joSB.put("note", "");
                    joSB.put("products", jaP);
                    respon = wu.requestHTTP("POST", url, joSB, mapHeader);
                    JSONObject joResultSO = new JSONObject(respon);
                    if (joResultSO.getString("status").equalsIgnoreCase("success")) {
                        JSONObject joDataSO = joResultSO.getJSONObject("data");
                        JSONObject joSale = joDataSO.getJSONObject("sale");
                        sales_booking_id = joSale.getInt("id");
                    } else {
                        return ResponseData.errorResponse("cannot insert sales booking!");
                    }
                } else {
                    return ResponseData.errorResponse("cannot get token!");
                }

            }

            LinkedHashMap<String, Object> map = new LinkedHashMap<>();
            map.put("c_order_id", o.get_ID());
            map.put("documentno", o.getDocumentNo());
            map.put("docstatus", o.getDocStatus());
            map.put("sales_booking_id", sales_booking_id);
            listData.add(map);
            resp = ResponseData.successResponse("", listData);
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (JSONException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }

        return resp;
    }

    public ResponseData syncProducts() {
        listData.clear();
        WsUtil wu = new WsUtil();
        String url = "https://qp.forca.id/api/v1/distributor/auth/login";
        Map<String, String> mapHeader = new LinkedHashMap<>();
        mapHeader.put("Content-Type", "aplication/json");
        mapHeader.put("Accept", "aplication/json");
        JSONObject joUser = new JSONObject();
        String token = "";
        StringBuilder spos_product_id = new StringBuilder();
        try {
            joUser.put("username", "developersisi2@gmail.com");
            joUser.put("password", "12345678");
            String respon = wu.requestHTTP("POST", url, joUser, mapHeader);
            JSONObject joLogin = new JSONObject(respon);
            if (joLogin.getString("status").equalsIgnoreCase("success")) {
                JSONObject joToken = joLogin.getJSONObject("data");
                token = joToken.getString("token");
                mapHeader.put("Forca-Token", token);
                Map<String, String> params = new LinkedHashMap<>();
                url = "https://qp.forca.id/api/v1/distributor/products/list_products";
                respon = wu.requestHTTP("GET",url, params, mapHeader);
                JSONObject joResponProduct = new JSONObject(respon);
                if (joResponProduct.getString("status").equalsIgnoreCase("success")) {
                    JSONObject joData = joResponProduct.getJSONObject("data");
                    JSONArray jaListProduct = joData.getJSONArray("list_products");
                    for (int a = 0; a < jaListProduct.length(); a++) {
                        JSONObject joProduct = jaListProduct.getJSONObject(a);
                        Integer id = Integer.valueOf(joProduct.getString("id"));
                        List<Object> listparams = new ArrayList();
                        //@formatter:off
                        String sql = 
                                " SELECT " + 
                                "    pos_product_id " + 
                                "FROM pos_product " + 
                                "WHERE pos_product_id_ref = ? ";
                        //@formatter:on
                        listparams.add(id);
                        int pos_product_id = DB.getSQLValueEx(null, sql, listparams);
                        X_POS_Product p = null;
                        if (pos_product_id <= 0) {
                            p = new X_POS_Product(Env.getCtx(), 0, null);
                        } else {
                            p = new X_POS_Product(Env.getCtx(), pos_product_id, null);
                        }
                        p.setPOS_Product_ID_Ref(id);
                        p.setCode(joProduct.getString("code"));
                        p.setName(joProduct.getString("name"));
                        p.setCategory(joProduct.getString("category_id"));
                        p.saveEx();
                        if (a > 0) {
                            spos_product_id.append(", ");
                        }
                        spos_product_id.append(p.get_ID());
                    }
                    LinkedHashMap<String, String> map = new LinkedHashMap<>();
                    map.put("pos_product_id", spos_product_id.toString());
                    listData.add(map);
                    resp = ResponseData.successResponse("", listData);
                } else {
                    return ResponseData.errorResponse("Cannot get list product!");
                }
            } else {
                return ResponseData.errorResponse("Cannot get token!");
            }
        } catch (JSONException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }
        return resp;
    }

}
