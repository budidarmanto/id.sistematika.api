package id.sistematika.api.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import id.sistematika.api.filter.AuthenticationApi;
import id.sistematika.api.filter.WsLogin;
import id.sistematika.api.model.MSISWsToken;
import id.sistematika.api.request.ParamRequest;
import id.sistematika.api.response.ResponseData;
import id.sistematika.api.util.Constants;

public class AuthenticationImpl {
    public static Properties wsenv = new Properties();
    public List<Object> listdata = new ArrayList<Object>();
    ResponseData resp = new ResponseData();

    public static Properties getDefaultCtx() {
        wsenv.setProperty("#AD_Language", "en_US");
        wsenv.setProperty("context", "deverp.semenindonesia.com");
        wsenv.setProperty("#AD_CLIENT_ID", "0");
        return wsenv;
    }


    @SuppressWarnings("unchecked")
    public ResponseData login(String user, String password) {
        getDefaultCtx();

        if (user.isEmpty() || user == null || password.isEmpty() || password == null) {
            return ResponseData.parameterRequired();
        }

        KeyNamePair[] clients = null;

        WsLogin login = new WsLogin(wsenv);
        clients = login.getClients(user, password);

        StringBuilder errorMessage = new StringBuilder();
        Map<String, String> map = new LinkedHashMap<String, String>();
        if (clients == null) {
            return ResponseData.inconsistent();

        } else {
            wsenv.setProperty(Constants.CTX_CLIENT, clients[0].getID());
            MUser mUser = MUser.get(wsenv, user);
            if (mUser != null) {

                ParamRequest param = new ParamRequest();
                param.setUsername(user);
                param.setAd_user_id(mUser.getAD_User_ID());
                param.setPassword(password);
                param.setAd_client_id(mUser.getAD_Client_ID());

                Integer ad_role_id = 0;
                try {
                    Integer a = 0;
                    List<Object> respRole = (List<Object>) getRoles(param).getResultdata();
                    if (respRole.size() > 1) {
                        while (ad_role_id == 0) {
                            Map<String, String> mWh = (Map<String, String>) respRole.get(a);
                            ad_role_id = Integer.valueOf((String) mWh.get("ad_role_id"));
                            a += 1;
                        }
                    } else {
                        Map<String, String> mWh = (Map<String, String>) respRole.get(a);
                        ad_role_id = Integer.valueOf((String) mWh.get("ad_role_id"));
                    }
                    param.setAd_role_id(ad_role_id);
                } catch (Exception e) {
                    errorMessage.append("Failed to get ad_role_id \n ");
                }

                Integer ad_org_id = 0;
                try {
                    Integer c = 0;
                    List<Object> respOrg = (List<Object>) getOrgs(param).getResultdata();
                    if (respOrg.size() > 1) {
                        while (ad_org_id == 0) {
                            Map<String, String> mWh = (Map<String, String>) respOrg.get(c);
                            ad_org_id = Integer.valueOf((String) mWh.get("ad_org_id"));
                            c += 1;
                        }
                    } else {
                        Map<String, String> mWh = (Map<String, String>) respOrg.get(c);
                        ad_org_id = Integer.valueOf((String) mWh.get("ad_org_id"));
                    }

                    param.setAd_org_id(ad_org_id);
                } catch (Exception e) {
                    errorMessage.append("Failed to get ad_org_id \n ");
                }


                Integer m_warehouse_id = 0;
                try {
                    Integer b = 0;
                    List<Object> respWh = (List<Object>) getWarehouses(param).getResultdata();
                    if (respWh.size() > 1) {
                        while (m_warehouse_id == 0) {
                            Map<String, String> mWh = (Map<String, String>) respWh.get(b);
                            m_warehouse_id = Integer.valueOf((String) mWh.get("m_warehouse_id"));
                            b += 1;
                        }
                    } else {
                        Map<String, String> mWh = (Map<String, String>) respWh.get(b);
                        m_warehouse_id = Integer.valueOf((String) mWh.get("m_warehouse_id"));
                    }
                    param.setM_warehouse_id(m_warehouse_id);
                } catch (Exception e) {
                    m_warehouse_id = 0;
                    resp = ResponseData.errorResponse(e.getMessage());
                }

                map.put("ad_user_id", String.valueOf(mUser.getAD_User_ID()));
                map.put("ad_user_name", mUser.getName());
                map.put("ad_client_id", String.valueOf(mUser.getAD_Client_ID()));
                
                //@formatter:off
                String name = DB.getSQLValueStringEx(null, ""
                        + "SELECT "
                        + " name "
                        + "FROM "
                        + " ad_client "
                        + "WHERE "
                        + " ad_client_id = ?", mUser.getAD_Client_ID());
                //@formatter:on
                map.put("ad_client_name", name);
                
                map.put("ad_org_id", String.valueOf(ad_org_id));
                map.put("ad_role_id", String.valueOf(ad_role_id));
                map.put("salesrep_ID", String.valueOf(mUser.getAD_User_ID()));
                map.put("m_warehouse_ID", String.valueOf(m_warehouse_id));
                try {
                    Map<String, Object> mToken =
                            (Map<String, Object>) getToken(param).getResultdata();
                    map.put("token", (String) mToken.get("token"));
                } catch (AdempiereException e) {
                    resp = ResponseData.errorResponse(e.getMessage());
                } catch (Exception e) {
                    resp = ResponseData.errorResponse("Failed to get Token\n");
                }

            }

            if (errorMessage.length() == 0) {
                resp = ResponseData.successResponse("Data Found", map);
            } else {
                resp = ResponseData.errorResponse(errorMessage.toString(), map);
            }

            return resp;
        }
    }


    public ResponseData getClients(ParamRequest param) {
        String user = param.getUsername();
        String password = param.getPassword();

        try {
            if (user.equals("") || user == null || password.equals("") || password == null) {
                return ResponseData.parameterRequired();
            }

            KeyNamePair[] clients = null;

            WsLogin login = new WsLogin(wsenv);

            clients = login.getClients(user, password);
            if (clients == null) {
                return ResponseData.inconsistent();
            } else {
                List<Object> resultdata = new ArrayList<Object>();

                for (int i = 0; i < clients.length; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    map.put("ad_client_id", clients[i].getID());
                    map.put("client_name", clients[i].getName());
                    resultdata.add(map);
                }

                resp = ResponseData.successResponse("Data Found", resultdata);
            }
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }
        return resp;
    }


    public ResponseData getRoles(ParamRequest param) {
        String user = param.getUsername();
        Integer ad_client_id = param.getAd_client_id();

        try {
            if (!DB.isConnected()) {
                return ResponseData.noConnection();
            }
            if (user == null || ad_client_id == null) {
                return ResponseData.parameterRequired();
            }

            KeyNamePair clients = new KeyNamePair(ad_client_id, user);
            KeyNamePair[] roles = null;
            getDefaultCtx();
            wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
            wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));

            WsLogin login = new WsLogin(wsenv);
            roles = login.getRoles(user, clients);

            if (roles == null) {
                resp = ResponseData.errorResponse("Roles not found for this user");
            } else {
                List<Object> resultdata = new ArrayList<Object>();

                for (int i = 0; i < roles.length; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    map.put("ad_role_id", roles[i].getID());
                    map.put("role_name", roles[i].getName());
                    resultdata.add(map);
                }

                resp = ResponseData.successResponse("Data Found", resultdata);
            }
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }
        return resp;
    }


    public ResponseData getOrgs(ParamRequest param) {
        ResponseData resp = new ResponseData();
        String whereClause = " name = ? ";
        String user = param.getUsername();
        MUser muser = new Query(Env.getCtx(), MUser.Table_Name, whereClause, null)
                .setParameters(user).first();
        Integer ad_user_id = muser.get_ID();
        Integer ad_client_id = param.getAd_client_id();
        Integer ad_role_id = param.getAd_role_id();

        try {
            if (!DB.isConnected()) {
                return ResponseData.noConnection();
            }
            if (user == null || ad_client_id == null || ad_role_id == null) {
                return ResponseData.parameterRequired();
            }

            KeyNamePair roles = new KeyNamePair(ad_role_id, "");
            KeyNamePair[] orgs = null;
            getDefaultCtx();
            wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
            wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
            wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
            wsenv.setProperty(Constants.CTX_ROLE, String.valueOf(ad_role_id));

            WsLogin login = new WsLogin(wsenv);
            orgs = login.getOrgs(roles);

            if (orgs == null) {
                resp = ResponseData.errorResponse("Organization not found for this role");
            } else {
                List<Object> resultdata = new ArrayList<Object>();

                for (int i = 0; i < orgs.length; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    map.put("ad_org_id", orgs[i].getID());
                    map.put("org_name", orgs[i].getName());
                    resultdata.add(map);
                }

                resp = ResponseData.successResponse("Data Found", resultdata);
            }
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }

        return resp;
    }


    public ResponseData getWarehouses(ParamRequest param) {
        String user = param.getUsername();
        Integer ad_client_id = param.getAd_client_id();
        Integer ad_org_id = param.getAd_org_id();

        try {
            if (!DB.isConnected()) {
                return ResponseData.noConnection();
            }
            if (user == null || ad_client_id == null || ad_org_id == null) {
                return ResponseData.parameterRequired();
            }

            KeyNamePair[] whouse = null;

            getDefaultCtx();
            wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
            wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
            wsenv.setProperty(Constants.CTX_ORG, String.valueOf(ad_org_id));
            Env.setCtx(wsenv);

            WsLogin login = new WsLogin(wsenv);
            whouse = login.getWarehouses(new KeyNamePair(ad_org_id, ""));

            if (whouse == null) {
                resp = ResponseData.errorResponse("Warehouse not found for this role");
            } else {

                List<Object> resultdata = new ArrayList<Object>();

                for (int i = 0; i < whouse.length; i++) {
                    Map<String, String> map = new LinkedHashMap<String, String>();
                    map.put("m_warehouse_id", whouse[i].getID());
                    map.put("warehouse_name", whouse[i].getName());
                    resultdata.add(map);
                }

                resp = ResponseData.successResponse("Data Found", resultdata);
            }
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }

        return resp;
    }



    public ResponseData getToken(ParamRequest param) {
        String username = param.getUsername();
        String password = param.getPassword();
        Integer ad_client_id = param.getAd_client_id();
        Integer ad_role_id = param.getAd_role_id();
        Integer ad_org_id = param.getAd_org_id();
        Integer m_warehouse_id = param.getM_warehouse_id();
        KeyNamePair[] users = null;

        try {
            if (!DB.isConnected()) {

                return ResponseData.noConnection();
            }
            if (username == null || password == null || ad_client_id == null || ad_org_id == null
                    || ad_role_id == null) {
                return ResponseData.parameterRequired();
            }

            Timestamp exptime = new Timestamp(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(exptime.getTime());
            cal.add(Calendar.MONTH, 1);
            exptime = new Timestamp(cal.getTime().getTime());

            WsLogin login = new WsLogin(getDefaultCtx());
            users = login.getAD_User(username, password);
            if (m_warehouse_id == null) {
                m_warehouse_id = 0;
            }
            AuthenticationApi.setContext(Integer.parseInt(users[0].getID()), ad_client_id,
                    ad_role_id, ad_org_id, m_warehouse_id);

            MSISWsToken wstoken = new MSISWsToken(Env.getCtx(),0, null);
            wstoken.setAD_Org_ID(ad_org_id);
            wstoken.setAD_Role_ID(ad_role_id);
            wstoken.setAD_User_ID(Integer.parseInt(users[0].getID()));
            if (m_warehouse_id != 0) {
                wstoken.setM_Warehouse_ID(m_warehouse_id);
            }
            wstoken.setExpiredDate(exptime); // expired 1 bulan
            if (wstoken.save()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("token", wstoken.getSIS_WS_Token_UU());
                map.put("ad_user_id", users[0].getID());
                map.put("expired", wstoken.getExpiredDate().toString());
                resp.setCodestatus("S");
                resp.setMessage("Success");
                resp.setResultdata(map);
            } else {
                resp = ResponseData
                        .errorResponse("Cannot create token, check the value of parameter");
            }
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }

        return resp;
    }


    public ResponseData refreshToken(ParamRequest param) {
        String token = param.getToken();

        try {
            if (!DB.isConnected()) {
                return ResponseData.noConnection();
            }
            if (token == null) {
                return ResponseData.parameterRequired();
            }

            Timestamp exptime = new Timestamp(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(exptime.getTime());
            cal.add(Calendar.MONTH, 1);
            exptime = new Timestamp(cal.getTime().getTime());

            AuthenticationApi.setContextByToken(token);

            String prmtr = "SIS_WS_Token_UU = '" + token + "' ";
            Query q = new Query(getDefaultCtx(), MSISWsToken.Table_Name, prmtr, null);
            q.setOnlyActiveRecords(true);
            MSISWsToken mwstoken = q.first();

            if (mwstoken.getSIS_WS_Token_ID() > 0) {
            	MSISWsToken newtoken = new MSISWsToken(Env.getCtx(),0, null);
                newtoken.setAD_Org_ID(mwstoken.getAD_Client_ID());
                newtoken.setAD_Role_ID(mwstoken.getAD_Role_ID());
                newtoken.setAD_User_ID(mwstoken.getAD_User_ID());
                newtoken.setM_Warehouse_ID(mwstoken.getM_Warehouse_ID());
                newtoken.setExpiredDate(exptime); // expired 1 bulan
                if (newtoken.save()) {
                    Map<String, Object> map = new LinkedHashMap<String, Object>();
                    map.put("token", newtoken.getSIS_WS_Token_UU());
                    map.put("ad_client_id", newtoken.getAD_Client_ID());
                    map.put("ad_role_id", newtoken.getAD_Role_ID());
                    map.put("ad_org_id", newtoken.getAD_Org_ID());
                    map.put("ad_user_id", newtoken.getAD_User_ID());
                    map.put("expired", newtoken.getExpiredDate());

                    resp = ResponseData.successResponse("Data Found", map);
                } else {
                    resp.setMessage("Cannot create token, check the value of parameter");
                }
            } else {
                resp = ResponseData.errorResponse("No valid token");
            }
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }

        return resp;
    }


    public ResponseData infoToken(ParamRequest param) {
        String token = param.getToken();

        try {
            if (!DB.isConnected()) {
                return ResponseData.noConnection();
            }
            if (token == null) {
                return ResponseData.parameterRequired();
            }

            AuthenticationApi.setContextByToken(token);

            String prmtr = "SIS_WS_Token_UU = '" + token + "' ";
            Query q = new Query(Env.getCtx(), MSISWsToken.Table_Name, prmtr, null);
            q.setOnlyActiveRecords(true);
            MSISWsToken mwstoken = q.first();

            if (mwstoken != null) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("token", mwstoken.getSIS_WS_Token_UU());
                map.put("ad_client_id", mwstoken.getAD_Client_ID());
                map.put("ad_role_id", mwstoken.getAD_Role_ID());
                map.put("ad_org_id", mwstoken.getAD_Org_ID());
                map.put("ad_user_id", mwstoken.getAD_User_ID());
                map.put("expired", mwstoken.getExpiredDate());
                map.put("hit", mwstoken.getHit());
                map.put("success", mwstoken.getSuccess());
                map.put("error", mwstoken.getErrorRequest());

                resp = ResponseData.successResponse("Data Found", map);
            } else {
                resp = ResponseData.errorResponse("Token not found");
            }
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }

        return resp;
    }

    public ResponseData logout(ParamRequest param) {
        String token = param.getToken();

        String whereCond = "SIS_WS_Token_UU = ? ";

        try {
            if (!DB.isConnected()) {
                return ResponseData.noConnection();
            }
            if (token == null) {
                return ResponseData.parameterRequired();
            }

            AuthenticationApi.setContextByToken(token);

            MSISWsToken wt = new Query(Env.getCtx(), MSISWsToken.Table_Name, whereCond, null)
                    .setParameters(token).first();
            wt.setIsActive(false);
            wt.saveEx();

            resp = ResponseData.successResponse("you have been logout", null);
        } catch (AdempiereException e) {
            resp = ResponseData.errorResponse(e.getMessage());
        } catch (Exception e) {
            resp = ResponseData.errorResponse(e.getMessage());
        }

        return resp;
    }
}
