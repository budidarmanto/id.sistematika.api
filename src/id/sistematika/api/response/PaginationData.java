/*----------------------------------------------------------------------------
   Product        : SIS ERP Foundation                                
   Author         : Mukhamad Wiwid Setiawan                                                                                
   Email          : m.wiwid.s@gmail.com                                                                 
   Subject        : Pagination Data                                
  ---------------------------------------------------------------------------*/

package id.sistematika.api.response;

import java.util.ArrayList;

public class PaginationData extends ResponseData {
    Object pagination = new ArrayList<Object>();
    
    public PaginationData() {
        super();
    }

    public void setPagination(Object pagination) {
        this.pagination = pagination;
    }
}
