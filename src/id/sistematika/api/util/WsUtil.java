package id.sistematika.api.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPGroup;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCurrency;
import org.compiere.model.MDocType;
import org.compiere.model.MPriceList;
import org.compiere.model.MRefList;
import org.compiere.model.MTax;
import org.compiere.model.MUser;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.json.JSONObject;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

public class WsUtil {
    static DateFormat formatter;

    public static Timestamp convertFormatTgl(String tgle) {
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        Timestamp xtgl = null;
        try {
            Date date = formatter.parse(tgle);
            xtgl = new Timestamp(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return xtgl;
    }

    public static Boolean convertYesNo(String val) {
        if (val.equalsIgnoreCase("N")) {
            return Boolean.valueOf(false);
        }
        return Boolean.valueOf(true);
    }

    public static int defaultBPGroup(Properties ctx) {
        int c_bpgrup_id = 0;
        String prmtr = "AD_Client_ID = " + Env.getAD_Client_ID(ctx) + " AND name like 'Standard'";
        Query q = new Query(Env.getCtx(), "C_BP_Group", prmtr, null);

        MBPGroup bpgrup = (MBPGroup) q.first();
        if (bpgrup.getC_BP_Group_ID() > 0) {
            c_bpgrup_id = bpgrup.getC_BP_Group_ID();
        }
        return c_bpgrup_id;
    }

    public static int defaultWarehouse(Properties ctx) {
        int whse_id = 0;
        String prmtr = "AD_Client_ID = ? AND name=? ";
        Query q = new Query(Env.getCtx(), "M_Warehouse", prmtr, null);
        q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(ctx)), "Standard"});
        q.setOnlyActiveRecords(true);
        MWarehouse whse = (MWarehouse) q.first();
        if (whse.getM_Warehouse_ID() > 0) {
            whse_id = whse.getM_Warehouse_ID();
        }
        return whse_id;
    }

    public static int defaultWarehouseSIS(Properties ctx) {
        int whse_id = 0;
        String prmtr = "AD_Client_ID = ? AND name=? ";
        Query q = new Query(Env.getCtx(), "M_Warehouse", prmtr, null);
        q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(ctx)), "SIS"});
        q.setOnlyActiveRecords(true);
        MWarehouse whse = (MWarehouse) q.first();
        if (whse.getM_Warehouse_ID() > 0) {
            whse_id = whse.getM_Warehouse_ID();
        }
        return whse_id;
    }

    public static int defaultPriceJual(Properties ctx) {
        int prc_id = 0;
        String prmtr = "AD_Client_ID = ? and IsSOPriceList=? ";
        Query q = new Query(Env.getCtx(), "M_PriceList", prmtr, null);
        q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(ctx)), "Y"});
        q.setOnlyActiveRecords(true);
        MPriceList prc = (MPriceList) q.first();
        if (prc.getM_PriceList_ID() > 0) {
            prc_id = prc.getM_PriceList_ID();
        }
        return prc_id;
    }

    public static String descDocStatus(Properties ctx, String kode) {
        String ref_desc = "";

        String prmtr = "AD_Client_ID = ? and AD_Reference_ID=? and value = ? ";
        Query q = new Query(Env.getCtx(), "AD_Ref_List", prmtr, null);
        q.setParameters(new Object[] {Integer.valueOf(0), Integer.valueOf(131), kode});
        q.setOnlyActiveRecords(true);
        MRefList prc = (MRefList) q.first();
        if (prc.getAD_Ref_List_ID() > 0) {
            ref_desc = prc.getName();
        }
        return ref_desc;
    }


    public static LinkedHashMap<String, String> docStatus(Properties ctx) {
        LinkedHashMap<String, String> docStatus = new LinkedHashMap<>();
        String prmtr = "AD_Client_ID = ? and AD_Reference_ID=? ";
        List<MRefList> listDocStatus = new Query(ctx, MRefList.Table_Name, prmtr, null)
                .setParameters(Integer.valueOf(0), Integer.valueOf(131)).list();
        for (MRefList mRefList : listDocStatus) {
            docStatus.put(mRefList.getValue(), mRefList.getName());
            docStatus.put(mRefList.getName(), mRefList.getValue());
        }
        return docStatus;
    }


    public static Integer getDefaultTaxID(Properties ctx) {
        String whereClause = " AD_Client_ID = ? AND Name = ?";
        MTax tax = new Query(ctx, MTax.Table_Name, whereClause, null)
                .setParameters(Env.getAD_Client_ID(ctx), "VZ").first();
        if (tax == null) {
            whereClause = " AD_Client_ID = ?";
            tax = new Query(ctx, MTax.Table_Name, whereClause, null)
                    .setParameters(Env.getAD_Client_ID(ctx)).first();
        }
        return tax.get_ID();
    }

    public static Integer getDefaultWarehouseID(Properties ctx) {
        String whereClause = " AD_Client_ID = ? AND Name = ?";
        MWarehouse warehouse = new Query(ctx, MWarehouse.Table_Name, whereClause, null)
                .setParameters(Env.getAD_Client_ID(ctx), "Standard").first();
        if (warehouse == null) {
            whereClause = " AD_Client_ID = ?";
            warehouse = new Query(ctx, MWarehouse.Table_Name, whereClause, null)
                    .setParameters(Env.getAD_Client_ID(ctx)).first();
        }
        return warehouse.get_ID();
    }

    public static Integer getDefaultDocTypeID(Properties ctx) {
        String whereClause = " Name = 'Standard Order' AND AD_Client_ID = ? ";
        MDocType docType = new Query(Env.getCtx(), MDocType.Table_Name, whereClause, null)
                .setParameters(Env.getAD_Client_ID(ctx)).first();
        if (docType == null) {
            whereClause = " AD_Client_ID = ? ";
            docType = new Query(Env.getCtx(), MDocType.Table_Name, whereClause, null)
                    .setParameters(Env.getAD_Client_ID(ctx)).first();
        }
        return docType.get_ID();
    }

    public static Integer getDefaultBPartnerLocationID(Integer C_BPartner_ID) {
        String whereClause = " C_BPartner_ID = ?";
        MBPartnerLocation bpLoc =
                new Query(Env.getCtx(), MBPartnerLocation.Table_Name, whereClause, null)
                        .setParameters(C_BPartner_ID).first();
        return bpLoc.get_ID();
    }

    public static Integer getDefaultCountryID() {
        String whereClause;
        whereClause = " ISO_Code = 'IDR' ";
        MCurrency cur = new Query(Env.getCtx(), MCurrency.Table_Name, whereClause, null).first();
        return cur.get_ID();
    }

    public static Integer getDefaultSalesRepIDByName(Properties ctx, String name) {
        name = "%" + name + "%";
        String whereClause = " lower(Name) like lower(?) AND AD_Client_ID = ? ";
        MUser salesrep = new Query(Env.getCtx(), MUser.Table_Name, whereClause, null)
                .setParameters(name, Env.getAD_Client_ID(ctx)).first();
        if (salesrep == null) {
            whereClause = "AD_Client_ID = ? ";
            salesrep = new Query(Env.getCtx(), MUser.Table_Name, whereClause, null)
                    .setParameters(Env.getAD_Client_ID(ctx)).first();
        }
        return salesrep.get_ID();
    }

    public static String getPaymentRule() {
        String result = "";

        // @formatter:off
        String sql = 
                "select " + 
                "    b.ad_ref_list_id " + 
                "from ad_reference a " + 
                "inner join ad_ref_list b on b.ad_reference_id = a.ad_reference_id " + 
                "where lower(a.name) like lower('%_Payment Rule%') " + 
                "and lower(b.name) like lower('%Check%')"
                ;
        // @formatter:on

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql, null);
            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString("ad_ref_list_id");
            }
        } catch (SQLException e) {

        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return result;
    }

    public String getHTML(String url, Map<String, String> params, Map<String, String> mapHeader)
            throws Exception {
        StringBuilder result = new StringBuilder();
        BufferedReader rd = null;
        HttpURLConnection conn = null;
        try {
            StringBuilder urls = new StringBuilder();
            urls.append(url);

            int count = 0;
            for (String key : params.keySet()) {
                if (count == 0) {
                    urls.append("?");
                } else {
                    urls.append("&");
                }
                urls.append(key).append("=").append(URLEncoder.encode(params.get(key), "UTF-8"));
                count++;
            }

            URL oUrl = new URL(urls.toString());
            conn = (HttpURLConnection) oUrl.openConnection();
            conn.setRequestMethod("GET");
            for (String key : mapHeader.keySet()) {
                conn.setRequestProperty(key, mapHeader.get(key));
            }

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } finally {
            rd.close();
            conn.disconnect();
        }
        return result.toString();
    }

    public String requestFormEncode(String requestMethod, String url, Map<String, String> params,
            Map<String, String> mapHeader) throws IOException {
        StringBuilder result = new StringBuilder();
        HttpURLConnection con = null;
        StringBuilder urls = new StringBuilder();
        int count = 0;
        for (String key : params.keySet()) {
            if (count > 0) {
                urls.append("&");
            }
            urls.append(key).append("=").append(URLEncoder.encode(params.get(key), "UTF-8"));
            count++;
        }
        byte[] postData = urls.toString().getBytes(StandardCharsets.UTF_8);
        try {

            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();

            con.setDoOutput(true);
            con.setRequestMethod(requestMethod);
            for (String key : mapHeader.keySet()) {
                con.setRequestProperty(key, mapHeader.get(key));
            }
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }
            try (BufferedReader br =
                    new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String line;
                result = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    result.append(line);
                    result.append(System.lineSeparator());
                }
            }
        } finally {
            con.disconnect();
        }
        return result.toString();
    }

    public String requestJSON(String requestMethod, String url, JSONObject jo,
            Map<String, String> mapHeader) throws IOException {
        StringBuilder result = new StringBuilder();
        HttpURLConnection con = null;
        try {

            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();

            con.setRequestMethod(requestMethod);
            for (String key : mapHeader.keySet()) {
                con.setRequestProperty(key, mapHeader.get(key));
            }

            con.setDoOutput(true);
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.writeBytes(jo.toString());
            }
            try (BufferedReader br =
                    new BufferedReader(new InputStreamReader(con.getInputStream()))) {
                String line;
                result = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    result.append(line);
                    result.append(System.lineSeparator());
                }
            }
        } finally {
            con.disconnect();
        }
        return result.toString();
    }

    public String requestHTTP(String requestMethod, String url, Object o,
            Map<String, String> mapHeader) {
        String result = "";
        OkHttpClient client = new OkHttpClient();
        StringBuilder uri = new StringBuilder();

        if (o != null) {
            if (o instanceof Map) {
                try {
                    Map<String, String> map = (Map<String, String>) o;
                    int count = 0;
                    for (String key : map.keySet()) {
                        if (count > 0) {
                            uri.append("&");
                        }
                        String value = map.get(key);
                        if (((String) mapHeader.get("Content-Type"))
                                .equalsIgnoreCase("application/x-www-form-urlencoded")) {
                            value = URLEncoder.encode(value, "UTF-8");
                        }
                        uri.append(key).append("=").append(value);
                        count++;
                    }
                } catch (UnsupportedEncodingException e) {
                    throw new AdempiereException(e.getMessage());
                }
            } else if (o instanceof JSONObject) {
                JSONObject jo = (JSONObject) o;
                uri.append(jo.toString());
            }
        }

        MediaType mediaType = null;
        if (mapHeader.get("Content-Type") != null) {
            if (((String) mapHeader.get("Content-Type")).equalsIgnoreCase("application/json")) {
                mediaType = MediaType.parse("application/json");
            } else if (((String) mapHeader.get("Content-Type"))
                    .equalsIgnoreCase("application/x-www-form-urlencoded")) {
                mediaType = MediaType.parse("application/x-www-form-urlencoded");
            }
        } else {
            if(uri.length() > 0) {
                url = url + "?" + uri.toString();
            }
        }

        RequestBody body = null;
        if (uri.length() > 0) {
            body = RequestBody.create(mediaType, uri.toString());
        }

        Builder rb = new Request.Builder().url(url);
        if (requestMethod.equalsIgnoreCase("POST")) {
            rb.post(body);
        } else if (requestMethod.equalsIgnoreCase("GET")) {
            rb.get();
        } else if (requestMethod.equalsIgnoreCase("PUT")) {
            rb.put(body);
        }
        for (String key : mapHeader.keySet()) {
            rb.addHeader(key, mapHeader.get(key));
        }
        Request request = rb.build();

        try {
            Response response = client.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            throw new AdempiereException(e.getMessage());
        }
        return result;
    }
}
