package id.sistematika.api.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.compiere.model.MBPGroup;
import org.compiere.model.Query;
import org.compiere.util.Env;

public class WebserviceUtil {

	public WebserviceUtil() {
		
	}
	
	public static int defaultBPGroup(Properties ctx)
    {
      int c_bpgrup_id = 0;
      String prmtr = "AD_Client_ID = " + Env.getAD_Client_ID(ctx) + " AND name like 'Standard'";
      Query q = new Query(Env.getCtx(), "C_BP_Group", prmtr, null);
      
      MBPGroup bpgrup = (MBPGroup)q.first();
      if (bpgrup.getC_BP_Group_ID() > 0) {
        c_bpgrup_id = bpgrup.getC_BP_Group_ID();
      }
      return c_bpgrup_id;
    }
	
	public static Timestamp stringToTimeStamp(String sdate){
		Timestamp date = null;
		try {
			if(sdate != null && !sdate.equals("") )
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date parsedTimeStamp = dateFormat.parse(sdate);
				date = new Timestamp(parsedTimeStamp.getTime());			
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;		
	}
	
	public static Timestamp stringToTimeStamp2(String sdate){
        Timestamp date = null;
        try {
            if(sdate != null && !sdate.equals("") )
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date parsedTimeStamp = dateFormat.parse(sdate);
                date = new Timestamp(parsedTimeStamp.getTime());            
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;        
    }
	
	public static List<Object> convertObjectToList(Object obj) {
        List<Object> list = new ArrayList<>();
        if (obj.getClass().isArray()) {
            list = Arrays.asList((Object[])obj);
        } else if (obj instanceof Collection) {
            list = new ArrayList<>((Collection<?>)obj);
        }
        return list;
    }
    
    public static boolean isCollection(Object obj) {
        return obj.getClass().isArray() || obj instanceof Collection;
    }
	
}
