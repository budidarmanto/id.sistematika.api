package id.sistematika.api.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Tes {
    public static void main(String[] args) {
        WsUtil wu = new WsUtil();
        String url = "https://qa.forca.id:9696/api/ws/authentication/v1/login";
        Map<String, String> map = new HashMap<String, String>();
        Map<String, String> mapHeader = new HashMap<String, String>();

        mapHeader.put("Content-Type", MediaType.APPLICATION_JSON);
        mapHeader.put("Accept", MediaType.APPLICATION_JSON);

        String token =
                "U1pTU3lxalFKTjJzV2pDOU0vQmNaaUhDbThUYjZxY0NIZjA9OjpXm5BgUueE2RYRrfmFr9hYOjpg3nj/K3DeGIGNIY4=";
        mapHeader.put("Authorization", token);
        url = "https://qp.forca.id/api/aksestoko/customers";
        JSONObject joCustomer = new JSONObject();

        try {
            joCustomer.put("customer_code", "100002004");
            joCustomer.put("customer_name", "AMININ");
            joCustomer.put("customer_store", "AMININ");
            joCustomer.put("customer_email", "100002004@mail.com");
            joCustomer.put("customer_address", "JALAN RAYA WONOKUSUMO");
            joCustomer.put("customer_region", "2");
            joCustomer.put("customer_village", "-");
            joCustomer.put("customer_district", "-");
            joCustomer.put("customer_city", "SURABAYA");
            joCustomer.put("customer_province", "JAWA TIMUR");
            joCustomer.put("customer_postal_code", "-");
            joCustomer.put("customer_phone", "085336327440");
            joCustomer.put("customer_active", "1");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String respon = wu.requestHTTP("POST", url, joCustomer, mapHeader);


//        OkHttpClient client = new OkHttpClient();
//
//        okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, joCustomer.toString());
//        Request request = new Request.Builder()
//          .url("https://qp.forca.id/api/aksestoko/customers")
//          .post(body)
//          .addHeader("Authorization", "U1pTU3lxalFKTjJzV2pDOU0vQmNaaUhDbThUYjZxY0NIZjA9OjpXm5BgUueE2RYRrfmFr9hYOjpg3nj/K3DeGIGNIY4=")
//          .addHeader("Content-Type", "application/json")
//          .build();
//
//        Response response = null;
//        try {
//            response = client.newCall(request).execute();
//            System.out.println(response.body().string());
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        
       
        System.out.println(respon);
    }
}
