package id.sistematika.api.component;

import java.sql.ResultSet;
import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;
import id.sistematika.api.model.MSISWsToken;
import id.sistematika.api.model.X_POS_Product;

public class ModelFactory implements IModelFactory {

    @Override
    public Class<?> getClass(String tableName) {
        if (tableName.equalsIgnoreCase(MSISWsToken.Table_Name)) {
            return MSISWsToken.class;
        } 
        if (tableName.equalsIgnoreCase(X_POS_Product.Table_Name)) {
            return X_POS_Product.class;
        } 
        return null;
    }

    @Override
    public PO getPO(String tableName, int Record_ID, String trxName) {
        if (tableName.equalsIgnoreCase(MSISWsToken.Table_Name)) {
            return new MSISWsToken(Env.getCtx(), Record_ID, trxName);
        }
        if (tableName.equalsIgnoreCase(X_POS_Product.Table_Name)) {
            return new X_POS_Product(Env.getCtx(), Record_ID, trxName);
        }
        return null;
    }

    @Override
    public PO getPO(String tableName, ResultSet rs, String trxName) {
        if (tableName.equalsIgnoreCase(MSISWsToken.Table_Name)) {
            return new MSISWsToken(Env.getCtx(), rs, trxName);
        }
        if (tableName.equalsIgnoreCase(X_POS_Product.Table_Name)) {
            return new X_POS_Product(Env.getCtx(), rs, trxName);
        }
        return null;
    }

}
