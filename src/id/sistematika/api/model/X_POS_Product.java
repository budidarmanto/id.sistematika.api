/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package id.sistematika.api.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for POS_Product
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_POS_Product extends PO implements I_POS_Product, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20200512L;

    /** Standard Constructor */
    public X_POS_Product (Properties ctx, int POS_Product_ID, String trxName)
    {
      super (ctx, POS_Product_ID, trxName);
      /** if (POS_Product_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_POS_Product (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 7 - System - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_POS_Product[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Category.
		@param Category Category	  */
	public void setCategory (String Category)
	{
		set_Value (COLUMNNAME_Category, Category);
	}

	/** Get Category.
		@return Category	  */
	public String getCategory () 
	{
		return (String)get_Value(COLUMNNAME_Category);
	}

	/** Set Validation code.
		@param Code 
		Validation Code
	  */
	public void setCode (String Code)
	{
		set_Value (COLUMNNAME_Code, Code);
	}

	/** Get Validation code.
		@return Validation Code
	  */
	public String getCode () 
	{
		return (String)get_Value(COLUMNNAME_Code);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

	/** Set POS Product.
		@param POS_Product_ID POS Product	  */
	public void setPOS_Product_ID (int POS_Product_ID)
	{
		if (POS_Product_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_POS_Product_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_POS_Product_ID, Integer.valueOf(POS_Product_ID));
	}

	/** Get POS Product.
		@return POS Product	  */
	public int getPOS_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_POS_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Product ID POS.
		@param POS_Product_ID_Ref Product ID POS	  */
	public void setPOS_Product_ID_Ref (int POS_Product_ID_Ref)
	{
		set_Value (COLUMNNAME_POS_Product_ID_Ref, Integer.valueOf(POS_Product_ID_Ref));
	}

	/** Get Product ID POS.
		@return Product ID POS	  */
	public int getPOS_Product_ID_Ref () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_POS_Product_ID_Ref);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set POS_Product_UU.
		@param POS_Product_UU POS_Product_UU	  */
	public void setPOS_Product_UU (String POS_Product_UU)
	{
		set_Value (COLUMNNAME_POS_Product_UU, POS_Product_UU);
	}

	/** Get POS_Product_UU.
		@return POS_Product_UU	  */
	public String getPOS_Product_UU () 
	{
		return (String)get_Value(COLUMNNAME_POS_Product_UU);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}