package id.sistematika.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MSISWsToken extends X_SIS_WS_Token {

    private static final long serialVersionUID = 1L;

    public MSISWsToken(Properties ctx, ResultSet rs, String trxName) {
        super(ctx, rs, trxName);
    }

    public MSISWsToken(Properties ctx, int SIS_WS_Token_ID, String trxName) {
        super(ctx, SIS_WS_Token_ID, trxName);
    }

}
