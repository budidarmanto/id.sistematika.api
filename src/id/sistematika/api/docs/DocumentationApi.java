package id.sistematika.api.docs;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DocumentationApi extends HttpServlet{
    
    private static final long serialVersionUID = 3754310184636194485L;

    @Override
    public void init(ServletConfig config) throws ServletException {
		
	}
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException, ServletException {

		RequestDispatcher view = req.getRequestDispatcher("swaggerui/index.html");
		view.forward(req, resp);
    }
	
}
