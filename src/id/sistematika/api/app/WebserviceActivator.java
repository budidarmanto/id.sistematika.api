package id.sistematika.api.app;

import org.adempiere.plugin.utils.Incremental2PackActivator;
import org.osgi.framework.BundleContext;

public class WebserviceActivator extends Incremental2PackActivator {
static BundleContext bundleContext;
	
	@Override
	public void start(BundleContext context) throws Exception {
		bundleContext = context;
		super.start(context);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		bundleContext = null;
		super.stop(context);
	}
}
