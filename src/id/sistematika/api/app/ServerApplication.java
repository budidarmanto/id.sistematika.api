package id.sistematika.api.app;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;
import com.wordnik.swagger.config.ConfigFactory;
import com.wordnik.swagger.config.ScannerFactory;
import com.wordnik.swagger.config.SwaggerConfig;
import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
import com.wordnik.swagger.model.ApiInfo;
import com.wordnik.swagger.reader.ClassReaders;

import id.sistematika.api.path.Authentication_FormData_v1;
import id.sistematika.api.path.Master_FormData_v1;
import id.sistematika.api.path.Transaction_FormData_v1;

public class ServerApplication extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        // swagger Core Plugin
        s.add(com.wordnik.swagger.jaxrs.listing.ApiListingResource.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ResourceListingProvider.class);
        
        addResourcePath(s);
        swaggerConfiguration();
        
        return s;
    }

    private void addResourcePath(Set<Class<?>> resource) {
        resource.add(Authentication_FormData_v1.class);
        resource.add(Master_FormData_v1.class);
        resource.add(Transaction_FormData_v1.class);
    }

    private void swaggerConfiguration() {
        SwaggerConfig swaggerConfig = new SwaggerConfig();
        ConfigFactory.setConfig(swaggerConfig);

        swaggerConfig.setBasePath("/api/ws");
        ApiInfo info = new ApiInfo("SIS API Documentation",
                "RESTful API from SIS ERP<br>"
                        + "Please Contact administrator if you want to access it !!!",
                "http://sistematika.id/", "budiskom93@gmail.com", "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0.html");
        swaggerConfig.setApiVersion("0.0.1");
        swaggerConfig.setApiInfo(info);

        ScannerFactory.setScanner(new DefaultJaxrsScanner());
        ClassReaders.setReader(new DefaultJaxrsApiReader());
    }
    
}
