package id.sistematika.api.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
@Component
public class RestFilter implements Filter {

    // ParamRequest param ;
    public static HttpServletRequest httpServletRequest;

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
            FilterChain filterChain) throws IOException, ServletException {


        if (servletRequest instanceof HttpServletRequest) {
            httpServletRequest = (HttpServletRequest) servletRequest;

            // diperlukan jika memakai authorization header
            // String authCredentials = httpServletRequest
            // .getHeader(Constants.AUTH_HEADER);

            final HttpServletResponse response = (HttpServletResponse) servletResponse;
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Access-Control-Allow-Methods", " GET, POST");// GET, POST, DELETE,
                                                                             // PUT, PATCH, OPTIONS
            response.setHeader("Access-Control-Allow-Headers",
                    "Origin, Accept, x-auth-token, "
                            + "Content-Type, api_key, Access-Control-Request-Method,"
                            + "Authorization, Access-Control-Request-Headers");
            response.setHeader("Copyright-Apps", "Sistematika");

            filterChain.doFilter(servletRequest, response);
        }
    }

}
