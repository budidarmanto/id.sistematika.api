package id.sistematika.api.path;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import id.sistematika.api.filter.AuthenticationApi;
import id.sistematika.api.filter.RestFilter;
import id.sistematika.api.impl.MasterImpl;
import id.sistematika.api.request.ParamBPGroup;
import id.sistematika.api.request.ParamRequest;
import id.sistematika.api.response.ResponseData;
import id.sistematika.api.util.Constants;;

@Component
@Path("/master/v1")
@Api(position = 1, value = "Master", description = "Master Data in SIS")
public class Master_FormData_v1 {
	
	@POST
	@Path("/getBPGroup")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(position = 1, httpMethod = "POST", value = "application/x-www-form-urlencoded", notes = "Use this method to retrieve one or many business partner group data")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "SIS-Token", value = "Token SIS", required = true, dataType = "string", paramType = "header") })
	public Response getBPGroup(
			@ApiParam(value = "Unique code for identifier business partner group data", required = false) @FormParam("c_bp_group_id") Integer bp_group_id) {

		ResponseData result = new ResponseData();
		HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
		String token = httpServletRequest.getHeader(Constants.AUTH_SIS_TOKEN);
		result = ResponseData.cekToken(token);

		if (result.getCodestatus().equals("S")) {
			ParamRequest param = new ParamRequest();
			param.setC_bp_group_id(bp_group_id);

			AuthenticationApi.setContextByToken(token);
			MasterImpl master = new MasterImpl();
			result = master.getBPGroup(param);
		}
		return ResponseData.finalResponse(result);
	}
	
	@POST
	@Path("/getBPGroupJSON")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(position = 2, httpMethod = "POST", value = "application/json", notes = "Use this method to retrieve one or many business partner group data")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "SIS-Token", value = "Token SIS", required = true, dataType = "string", paramType = "header") })
	public Response getBPGroupJSON(ParamBPGroup param) {

		ResponseData result = new ResponseData();
		HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
		String token = httpServletRequest.getHeader(Constants.AUTH_SIS_TOKEN);
		result = ResponseData.cekToken(token);

		if (result.getCodestatus().equals("S")) {
			AuthenticationApi.setContextByToken(token);
			MasterImpl master = new MasterImpl();
			result = master.getBPGroupJSON(param);
		}
		return ResponseData.finalResponse(result);
	}

}
