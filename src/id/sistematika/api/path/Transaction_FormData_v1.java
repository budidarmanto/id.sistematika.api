package id.sistematika.api.path;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.Trx;
import org.springframework.stereotype.Component;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import id.sistematika.api.filter.AuthenticationApi;
import id.sistematika.api.filter.RestFilter;
import id.sistematika.api.impl.MasterImpl;
import id.sistematika.api.impl.TransactionImpl;
import id.sistematika.api.request.ParamBPGroup;
import id.sistematika.api.request.ParamOrder;
import id.sistematika.api.request.ParamRequest;
import id.sistematika.api.response.ResponseData;
import id.sistematika.api.util.Constants;;

@Component
@Path("/transaction/v1")
@Api(position = 1, value = "Transaction", description = "Transaction Data in SIS")
public class Transaction_FormData_v1 {
	
	@POST
	@Path("/setSalesOrder")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(position = 1, httpMethod = "POST", value = "application/x-www-form-urlencoded", notes = "Use this method to retrieve one or many business partner group data")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "SIS-Token", value = "Token SIS", required = true, dataType = "string", paramType = "header") })
	public Response setSalesOrder(
			ParamOrder param) {

		ResponseData result = new ResponseData();
		HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
		String token = httpServletRequest.getHeader(Constants.AUTH_SIS_TOKEN);
		result = ResponseData.cekToken(token);

		if (result.getCodestatus().equals("S")) {
			AuthenticationApi.setContextByToken(token);
			TransactionImpl trans = new TransactionImpl();
			String trxName = Trx.createTrxName("setSalesOrder");
			Trx trxLocal = Trx.get(trxName, true);
			try {
			    result = trans.setSalesOrder(param, trxLocal);
            } catch (AdempiereException e) {
                trxLocal.rollback();
            } catch (Exception e) {
                trxLocal.rollback();
            }finally {
                trxLocal.commit();
                if (trxLocal.isActive()) {
                    trxLocal.close();
                }
            }
		}
		return ResponseData.finalResponse(result);
	}
	
	@POST
    @Path("/syncProducts")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 2, httpMethod = "POST", value = "application/x-www-form-urlencoded", notes = "Use this method to retrieve one or many business partner group data")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "SIS-Token", value = "Token SIS", required = true, dataType = "string", paramType = "header") })
    public Response syncProducts() {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_SIS_TOKEN);
        result = ResponseData.cekToken(token);

        if (result.getCodestatus().equals("S")) {

            AuthenticationApi.setContextByToken(token);
            TransactionImpl trans = new TransactionImpl();
            result = trans.syncProducts();
        }
        return ResponseData.finalResponse(result);
    }

}
